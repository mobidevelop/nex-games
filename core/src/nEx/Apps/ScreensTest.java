/*
 * Copyright (C) 2012 Justin Shapcott 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package nEx.Apps;

import nEx.Games.Screens.Screen;
import nEx.Games.Screens.ScreenManager;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * A simple test and usage example of the Screens package.
 * <p>
 * This test creates and displays two screens.
 * <p>
 * Clicking three times will cause the top screen
 * to be lowered to the bottom (and display a {@link Toast}).
 * <p>
 * Clicking three more times will cause the top screen
 * to be exited (and display a {@link Toast}).
 * <p>
 * Clicking three more times will cause the final screen
 * to be exited (and display a {@link Toast}), exit the app.
 *   
 * @author jshapcot
 */
public class ScreensTest extends ApplicationAdapter {

	ScreenManager mManager;
	BitmapFont mFont;
	SpriteBatch mBatch;
	
	@Override
	public void create() {
		
		mFont = new BitmapFont();
		mBatch = new SpriteBatch();
		
		mManager = new ScreenManager() {

			@Override
			public void render(float delta) {
				Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
				super.render(delta);
			}					
		};
		Screen screen1 = new Screen() {					
			{
				this.setTransitionInDuration(5.0f);
				this.setTransitionOutDuration(1.0f);
			}
			
			int touches = 0;
			
			@Override
			public void render(float delta) {
				switch(this.getState()) {
					case Shown: {
						if (this.isFocused()) {
							mFont.setColor(0, 1, 0, 1);	
						} else {
							mFont.setColor(1, 0, 0, 1);
						}
						break;
					}
					case TransitionIn: {
						mFont.setColor(1, 1, 1, 0 + (this.getStateTime() / this.getTransitionInDuration()));
						break;
					}
					case TransitionOut: {
						mFont.setColor(1, 1, 1, 1 - (this.getStateTime() / this.getTransitionOutDuration()));
						break;
					}
				}
				
				
				mFont.draw(mBatch, "Screen1:: " + this.getState().name() + " : " + this.getStateTime(), 10, mFont.getLineHeight() * 2);
				
				if (this.isFocused()) {
					if (Gdx.input.justTouched()) {
						Gdx.app.log("core", "Screen1: " + (touches + 1));
						if (++touches == 3) {
							this.getScreenManager().addScreen(new Toast("Screen1: exiting"));
							this.exit();
						}	
					}							
				}
			}
		};
		Screen screen2 = new Screen() {
			{
				this.setTransitionInDuration(2.5f);
				this.setTransitionOutDuration(1.0f);
			}

			int count = 0;
			int touches = 0;
			
			@Override
			public void render(float delta) {

				switch(this.getState()) {
					case Shown: {
						if (this.isFocused()) {
							mFont.setColor(0, 1, 0, 1);	
						} else {
							mFont.setColor(1, 0, 0, 1);
						}
						break;
					}
					case TransitionIn: {
						mFont.setColor(1, 1, 1, 0 + (this.getStateTime() / this.getTransitionInDuration()));
						break;
					}
					case TransitionOut: {
						mFont.setColor(1, 1, 1, 1 - (this.getStateTime() / this.getTransitionOutDuration()));
						break;
					}
				}
				mFont.draw(mBatch, "Screen2:: " + this.getState().name() + " : " + this.getStateTime(), 10, mFont.getLineHeight() * 4);
				
				if (this.isFocused()) {
					if (Gdx.input.justTouched()) {
						Gdx.app.log("core", "Screen2: " + (touches + 1));
						if (++touches == 3) {
							if (count == 0) {
								count = 1;
								touches = 0;										
								this.lowerToBottom();
								this.getScreenManager().addScreen(new Toast("Screen2: lowered to bottom", 3.0f));
							} else {
								this.getScreenManager().addScreen(new Toast("Screen2: exiting"));
								this.exit();	
							}
						}
					}							
				}
			}					
		};				
		mManager.addScreen(screen1);
		mManager.addScreen(screen2);
		screen1.show();
		screen2.show();
	}

	@Override
	public void render() {
		final float delta = Gdx.graphics.getDeltaTime();
		mManager.update(delta);
		mBatch.begin();
		mManager.render(delta);
		mBatch.end();
		if (mManager.getScreenCount() == 0) {
			Gdx.app.exit();
		}
	}
	
	public class Toast extends Screen {
		String message;
		float duration;
		
		public Toast(String message) {
			this(message, 1.0f);
		}
		public Toast(String message, float duration) {			
			this.message = message;
			this.duration = duration;
			this.setPopup(true);
			this.setFocusable(false);			
			this.setTransitionInDuration(0.25f);
			this.setTransitionOutDuration(0.25f);
			this.show();
		}
		@Override
		public void render(float delta) {
			switch(this.getState()) {
				case Shown: {
					mFont.setColor(1, 1, 1, 1);					
					break;
				}
				case TransitionIn: {
					mFont.setColor(1, 1, 1, 0 + (this.getStateTime() / this.getTransitionInDuration()));
					break;
				}
				case TransitionOut: {
					mFont.setColor(1, 1, 1, 1 - (this.getStateTime() / this.getTransitionOutDuration()));
					break;
				}
			}
			TextBounds bounds = mFont.getBounds(message);
			mFont.draw(mBatch, message, (Gdx.graphics.getWidth() - bounds.width) / 2, mFont.getLineHeight() * 2 + bounds.height);
			if (this.getState() == State.Shown && this.getStateTime() >= duration) {
				this.exit();
			}
		}
	}
}
