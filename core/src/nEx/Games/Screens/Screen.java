/*
 * Copyright (C) 2012 Justin Shapcott 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package nEx.Games.Screens;


/**
 * A Screen is a single layer that has update and draw logic.
 * <p>
 * When added to a {@link ScreenManager},
 * Screens can be stacked on top of each other then
 * updated and rendered as a group. 
 * 
 * @author jshapcot
 */
public class Screen {
	private ScreenManager mScreenManager = null;
	private State mState = State.Hidden;
	private float mStateTime = 0;
	private float mTransitionInDuration = 0;
	private float mTransitionOutDuration = 0;
	
	private boolean mFocusable = true;
	private boolean mFocused = false;
	private boolean mCovered = false;
	private boolean mPopup = false;
	private boolean mExiting = false;

	public Screen() {
		
	}
	
	
	public ScreenManager getScreenManager() {
		return mScreenManager;
	}
	
	
	public void setScreenManager(ScreenManager screenManager) {
		mScreenManager = screenManager;
	}
	
	/**
	 * Returns the {@link State} that this {@link Screen} is currently in.
	 *  
	 * @return state
	 */		
	public State getState() {
		return mState;
	}
	
	/**
	 * Sets the {@link State} that this {@link Screen} is currently in.
	 * Also resets the StateTime to 0.
	 *  
	 * @param state
	 */			
	public void setState(State state) {
		if (mState != state) {
			mState = state;			
			mStateTime = 0;
		}
	}
	
	/**
	 * Returns the length of time (in seconds) that this {@link Screen}
	 * has been in its current {@link State}.
	 *  
	 * @return time.
	 */	
	public float getStateTime() {
		return mStateTime;
	}
	
	/**
	 * Returns the length of time (in seconds) that this {@link Screen}
	 * takes to transition into the Shown {@link State}.
	 *  
	 * @return duration
	 */
	public float getTransitionInDuration() {
		return mTransitionInDuration;
	}
	
	/**
	 * Sets the length of time (in seconds) that this {@link Screen}
	 * takes to transition in into the Shown {@link State}.
	 * 
	 * @param duration
	 */	
	public void setTransitionInDuration(float duration) {
		mTransitionInDuration = duration;
	}
	
	/**
	 * Returns the length of time (in seconds) that this {@link Screen}
	 * takes to transition into the Hidden {@link State}.
	 *  
	 * @return duration
	 */	
	public float getTransitionOutDuration() {
		return mTransitionOutDuration;
	}
	
	/**
	 * Sets the length of time (in seconds) that this {@link Screen}
	 * takes to transition in into the Hidden {@link State}.
	 * 
	 * @param duration
	 */		
	public void setTransitionOutDuration(float duration) {
		mTransitionOutDuration = duration;
	}

	/**
	 * Returns whether this {@link Screen} can be focused.
	 * <p>
	 * This is used by the {@link ScreenManager} to determine
	 * if this {@link Screen} can be given focus.
	 *   
	 * @return focusable
	 */		
	public boolean isFocusable() {
		return mFocusable;
	}

	/**
	 * Sets whether this {@link Screen} can be focused.
	 * <p>
	 * Typically, a subclass will set this based on
	 * whether it needs to receive input.
	 * 
	 * @param focusable
	 */		
	public void setFocusable(boolean focusable) {
		mFocusable = focusable;
	}
	
	/**
	 * Returns whether this {@link Screen} currently has focus.
	 * <p>
	 * When used with a {@link ScreenManager} a screen will be
	 * focused when it is the topmost focusable screen.
	 * 
	 * @return focused
	 */			
	public boolean isFocused() {
		return mFocused;
	}
	
	/**
	 * Sets whether this {@link Screen} currently has focus.
	 * <p>
	 * When used with a {@link ScreenManager} a screen will be
	 * focused when it is the topmost focusable screen.
	 * 
	 * @param focused
	 */		
	public void setFocused(boolean focused) {
		mFocused = focused;
	}
	
	/**
	 * Returns whether this {@link Screen} is currently covered.
	 * <p>
	 * When used with a {@link ScreenManager} a screen will be
	 * covered when there is a {@link Screen} above it in the
	 * stack which is not a popup.
	 * <p>
	 * This lets the screen optimize its updating and rendering
	 * when it knows it is covered.
	 * 
	 * @return covered
	 */		
	public boolean isCovered() {
		return mCovered;
	}
	
	/**
	 * Returns whether this {@link Screen} is currently covered.
	 * <p>
	 * When used with a {@link ScreenManager} a screen will be
	 * covered when there is a {@link Screen} above it in the
	 * stack which is not a popup.
	 * <p>
	 * This lets the screen optimize its updating and rendering
	 * when it knows it is covered.
	 * 
	 * @param covered
	 */		
	public void setCovered(boolean covered) {
		mCovered = covered;
	}
	
	/**
	 * Returns whether this {@link Screen} is considered a popup.
	 * <p>
	 * When used with a {@link ScreenManager} a screen which is
	 * not a popup will cause any {@link Screen} lower in the
	 * stack to be covered.
	 * 
	 * @param popup
	 */		
	public boolean isPopup() {
		return mPopup;
	}

	/**
	 * Sets whether this {@link Screen} is considered a popup.
	 * <p>
	 * When used with a {@link ScreenManager} a screen which is
	 * not a popup will cause any {@link Screen} lower in the
	 * stack to be covered.
	 * 
	 * @param popup
	 */		
	public void setPopup(boolean popup) {
		mPopup = popup;
	}

	/**
	 * Returns whether this {@link Screen} is currently exiting.
	 * 
	 * @param popup
	 */			
	public boolean isExiting() {
		return mExiting;
	}
	
	/**
	 * Puts this {@link Screen} into the TransitionIn or Shown {@link State}.
	 * <p>
	 * When this {@link Screen} has a TransitionInDuration which is
	 * greater than zero, this will put it into the TransitionIn {@link State},
	 * otherwise it will be put into the Shown {@link State}.
	 *  
	 * @param transition : Whether or not to use the TransitionIn {@link State}
	 */
	public void show() {
		if (mTransitionInDuration > 0.0f) {
			setState(State.TransitionIn);
		} else {
			setState(State.Shown);
		}
	}
	
	/**
	 * Puts this {@link Screen} into the TransitionIn or Shown {@link State},
	 * depending on the value of transition.
	 * 
	 * @param transition : Whether or not to use the TransitionIn {@link State}
	 */	
	public void show(boolean transition) {
		if (transition && mTransitionInDuration > 0.0f) {
			setState(State.TransitionIn);
		} else {
			setState(State.Shown);
		}
	}
	
	/**
	 * Puts this {@link Screen} into the TransitionOut or Hidden {@link State}.
	 * <p>
	 * When this {@link Screen} has a TransitionOutDuration which is
	 * greater than zero, this will put it into the TransitionOut {@link State},
	 * otherwise it will be put into the Hidden {@link State}.
	 *  
	 * @param transition : Whether or not to use the TransitionIn {@link State}
	 */	
	public void hide() {
		if (mTransitionOutDuration > 0.0f) {
			setState(State.TransitionOut);
		} else {
			setState(State.Hidden);
		}
	}
	
	/**
	 * Puts this {@link Screen} into the TransitionOut or Hidden {@link State},
	 * depending on the value of transition.
	 * @param transition : Whether or not to use the TransitionOut {@link State}
	 */		
	public void hide(boolean transition) {
		if (transition && mTransitionOutDuration > 0.0f) {
			setState(State.TransitionOut);
		} else {
			setState(State.Hidden);
		}		
	}
	
	/**
	 * Removes this {@link Screen} from the {@link ScreenManager}.
	 * @param transition : Whether or not to use the TransitionOut {@link State}
	 */		
	public void exit() {
		mExiting = true;
		hide();
	}
	
	/**
	 * Removes this {@link Screen} from the {@link ScreenManager}.
	 * @param transition : Whether or not to use the TransitionOut {@link State}
	 */	
	public void exit(boolean transition) {
		mExiting = true;
		hide(transition);		
	}
	
	/**
	 * Moves this {@link Screen} up one position in its ScreenManager.
	 * <p>
	 * If this {@link Screen} does not have a ScreenManager,
	 * this will be a noop.
	 */		
	public void raise() {
		if (mScreenManager != null) {
			mScreenManager.raiseScreen(this);
		}
	}
	
	/**
	 * Moves this {@link Screen} down one position in its ScreenManager.
	 * <p>
	 * If this {@link Screen} does not have a ScreenManager,
	 * this will be a noop.
	 */
	public void lower() {
		if (mScreenManager != null) {
			mScreenManager.lowerScreen(this);
		}
	}
	
	/**
	 * Moves this {@link Screen} up to the top position in its ScreenManager.
	 * <p>
	 * If this {@link Screen} does not have a ScreenManager,
	 * this will be a noop.
	 */	
	public void raiseToTop() {
		if (mScreenManager != null) {
			mScreenManager.raiseScreenToTop(this);
		}
	}
	
	/**
	 * Moves this {@link Screen} down to the bottom position in its ScreenManager.
	 * <p>
	 * If this {@link Screen} does not have a ScreenManager,
	 * this will be a noop.
	 */
	public void lowerToBottom() {
		if (mScreenManager != null) {
			mScreenManager.lowerScreenToBottom(this);
		}
	}
	
	/**
	 * Updates this {@link Screen} changing its {@link State} when necessary.
	 * <p>
	 * Subclasses must call through to this method, otherwise the {@link State}
	 * and StateTime will not be updated.
	 */	
	public void update(float delta) {
		mStateTime += delta;
		switch(mState)
		{
			case TransitionIn:
			{
				if (mStateTime >= mTransitionInDuration)
				{
					setState(State.Shown);					
				}
				break;
			}			
			case TransitionOut:
			{
				if (mStateTime >= mTransitionOutDuration)
				{
					setState(State.Hidden);
					if (mExiting)
					{
						mScreenManager.removeScreen(this);
						mExiting = false;
					}
				}
				break;
			}
		}		
	}

	/**
	 * Renders this {@link Screen}.
	 * 
	 * @param delta    the time (in seconds) since the last render.
	 */
	public void render(float delta) {
		// Base Screen does nothing
	}

	/**
	 * The State of a {@link Screen} is used to determine
	 * how it should be updated and rendered.
	 * <p>
	 * When Shown, a {@link Screen} <b>will be</b> updated and rendered.
	 * <p>
	 * When Hidden, a {@link Screen} <b>will not be</b> updated and rendered.
	 * <p>
	 * TransitionIn is an intermediate state when moving from Hidden to Shown.
	 * <p>
	 * TransitionOut is an intermediate state when moving from Shown to Hidden.
	 */
	public enum State {
		Shown,
		Hidden,
		TransitionIn,
		TransitionOut,
	}	
}
