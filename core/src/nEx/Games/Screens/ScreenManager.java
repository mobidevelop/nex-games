/*
 * Copyright (C) 2012 Justin Shapcott 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package nEx.Games.Screens;

import java.util.*;

import nEx.Games.Screens.Screen.State;


/**
 * Manages a stack of screens.
 * 
 * @author jshapcot
 */
public class ScreenManager {
	
	private List<Screen> mScreens;
	private List<Screen> mScreensToUpdate;
	private List<Screen> mScreensToRender;
	
	public ScreenManager() {
		mScreens = new ArrayList<Screen>();
		mScreensToUpdate = new ArrayList<Screen>();
		mScreensToRender = new ArrayList<Screen>();
	}
	
	/**
	 * Instructs each {@link Screen} to update itself.
	 * Only screens which are not hidden will be updated. 
	 * 
	 * @param delta    the time (in seconds) since the last update.
	 */
	public void update(float delta) {
		boolean top = true;
		boolean covered = false;		
		mScreensToUpdate.addAll(mScreens);
		for (int i = mScreensToUpdate.size() - 1; i >= 0; --i) {
			Screen screen = mScreensToUpdate.get(i);
			if (screen.getState() != State.Hidden) {
				if (top && screen.isFocusable()) {
					screen.setFocused(true);
					top = false;
				} else {
					screen.setFocused(false);
				}
				screen.update(delta);
				if (!covered && !screen.isPopup()) {
					covered = true;
				}
			} else {
				screen.setFocused(false);
			}
		}
		mScreensToUpdate.clear();
	}
	
	
	/**
	 * Instructs each {@link Screen} to render itself.
	 * Only screens which are not hidden will be rendered. 
	 * 
	 * @param delta    the time (in seconds) since the last render.
	 */
	public void render(float delta) {
		mScreensToRender.addAll(mScreens);
		for (int i = mScreensToRender.size() - 1; i >= 0; --i) {
			Screen screen = mScreensToRender.get(i);
			if (screen.getState() != State.Hidden) {
				screen.render(delta);
			}
		}		
		mScreensToRender.clear();
	}
	/**
	 *  Adds the given {@link Screen} at the top of the stack.
	 *  
	 *  @param screen
	 */	
	public void addScreen(Screen screen) {
		if (mScreens.contains(screen) == false) {
			mScreens.add(screen);
			screen.setScreenManager(this);
		}
	}
	/**
	 *  Removes the given {@link Screen} from the stack.
	 *  
	 *  @param screen
	 */	
	public void removeScreen(Screen screen) {
		if (mScreens.contains(screen) != false) {
			mScreens.remove(screen);
			screen.setScreenManager(null);			
		}		
	}
	/**
	 *  Moves the given {@link Screen} up one position in the stack.
	 *  <p>
	 *  The topmost focusable screen will be given focus during the update
	 *    
	 *  @param screen
	 */		
	public void raiseScreen(Screen screen) {
		if (mScreens.contains(screen) != false) {
			int index = mScreens.indexOf(screen);
			if (index < mScreens.size() - 1) {
				mScreens.remove(index);
				mScreens.add(index + 1, screen);
			}
		}
	}
	/**
	 *  Lowers the given {@link Screen} up one position in the stack.
	 *  <p>
	 *  Screens which are lower in the stack will be told when they are covered
	 *  which will give them an opportunity to optimize their updating and/or rendering
	 *    
	 *  @param screen
	 */		
	public void lowerScreen(Screen screen) {
		if (mScreens.contains(screen) != false) {
			int index = mScreens.indexOf(screen);
			if (index > 0) {
				mScreens.remove(index);
				mScreens.add(index - 1, screen);
			}
		}
	}
	/**
	 *  Moves the given {@link Screen} up to the top of the stack.
	 *  <p>
	 *  The topmost focusable screen will be given focus during the update
	 *  	
	 *  @param screen
	 */		
	public void raiseScreenToTop(Screen screen) {
		if (mScreens.contains(screen) != false) {
			int index = mScreens.indexOf(screen);
			if (index < mScreens.size() - 1) {
				mScreens.remove(index);
				mScreens.add(screen);
			}
		}
	}
	/**
	 *  Moves the given {@link Screen} down to the bottom of the stack.
	 *  <p>
	 *  Screens which are lower in the stack will be told when they are covered
	 *  which will give them an opportunity to optimize their updating and/or rendering
	 *  
	 *  @param screen
	 */			
	public void lowerScreenToBottom(Screen screen) {
		if (mScreens.contains(screen) != false) {
			int index = mScreens.indexOf(screen);
			if (index > 0) {
				mScreens.remove(index);
				mScreens.add(0, screen);
			}
		}
	}
	
	/**
	 * Returns the count of Screens currently being managed by this ScreenManager
	 * @return count 
	 */
	public int getScreenCount() {
		return mScreens.size();
	}
}
