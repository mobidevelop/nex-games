# nEx.Games
This is a collection of "extensions" for games built with LibGDX.

Currently, the only module is the Screens package, which provides a Screen class for modularizing code and a ScreenManager class for managing a stack of Screens.
